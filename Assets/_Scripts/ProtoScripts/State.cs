using UnityEngine;
using UnityEngine.InputSystem;

public abstract class State
{
    public virtual void OnStateEnter()
    {
    }
    public virtual void OnStateExit()
    {
    }
    public virtual void OnUpdate()
    {
    }
}

public abstract class CCState : State
{
    protected Character2 owner;
    protected CharSound charSound;
    protected InputAction moveAction;
    protected InputAction camTurnLAction;
    protected InputAction camTurnRAction;
    protected InputAction jumpAction;
    protected float moveSpeed;
    protected float rotSpeed;
    protected Transform meshTransform;
    protected Transform camHolderTransform;
    protected CharacterController cc;
    protected float vSpeed;
    protected float jumpSpeed;
    protected float gravity = 9.81f;
    protected float friction;
    protected float camRotDuration;
    protected Vector3 velocity;
    protected bool isJumping;


    // Constructor
    public CCState(Character2 owner)
    {
        this.owner = owner;
        this.charSound = owner.charSound;
        this.moveAction = owner.moveAction;
        this.camTurnLAction = owner.camTurnLAction;
        this.camTurnRAction = owner.camTurnRAction;
        this.jumpAction = owner.jumpAction;
        this.moveSpeed = owner.moveSpeed;
        this.rotSpeed = owner.rotSpeed;
        this.meshTransform = owner.meshTransform;
        this.camHolderTransform = owner.camHolderTransform;
        this.cc = owner.cc;
        this.gravity = owner.gravity;
        this.friction = owner.friction;
        this.camRotDuration = owner.camRotDuration;
    }

    protected void Gravity()
    {
        if (cc.isGrounded)
        {
            vSpeed = 0f;
        }
        vSpeed += gravity * Time.deltaTime;
        velocity += Vector3.down * vSpeed * Time.deltaTime;
        //cc.Move(Vector3.down * vSpeed * Time.deltaTime);
    }

}

public class NormalState : CCState
{
    public NormalState(Character2 owner) : base(owner)
    {
    }

    public override void OnStateEnter()
    {
        moveAction.Enable();
        camTurnLAction.Enable();
        camTurnLAction.performed += ctx => { TurnCam(-90f); };
        camTurnRAction.Enable();
        camTurnRAction.performed += ctx => { TurnCam(90f); };
        jumpAction.Enable();
        jumpAction.performed += ctx => { Jump(); };
    }

    public override void OnStateExit()
    {
        moveAction.Disable();
        camTurnLAction.Disable();
        camTurnRAction.Disable();
        jumpAction.Disable();
    }


    public override void OnUpdate()
    {
        Move();
        Gravity();
    }

    void TurnCam(float rotAmount)
    {
        owner.camRotAmount = rotAmount;
        owner.SwitchState(owner.rotationState);
    }

    void Jump()
    {
        if (!cc.isGrounded) { return; }
        isJumping = true;
        //cc.Move(Vector3.up * jumpSpeed);
    }

    void Move()
    {
        if (Time.timeScale == 0) { return; }
        Vector3 forward = camHolderTransform.forward;
        Vector3 right = camHolderTransform.right;
        forward.y = 0f;
        right.y = 0f;
        forward.Normalize();
        right.Normalize();
        Vector2 moveVector = moveAction.ReadValue<Vector2>();
        Vector3 moveDir = forward * moveVector.y + right * moveVector.x;
        velocity += moveDir * moveSpeed * Time.deltaTime;
        if (isJumping)
        {
            jumpSpeed = gravity * 5f;
            velocity += Vector3.up * jumpSpeed * Time.deltaTime;
            isJumping = false;
        }
        cc.Move(velocity);
        velocity -= friction * Time.deltaTime * velocity;
        if (moveDir != Vector3.zero)
        {
            Quaternion toRot = Quaternion.LookRotation(moveDir, Vector3.up);
            meshTransform.rotation = Quaternion.RotateTowards(
                meshTransform.rotation,
                toRot,
                rotSpeed * Time.deltaTime);
        }
        charSound.UpdateFootsteps(moveDir.magnitude);
    }

}

public class RotationState : CCState
{
    float timer;
    float yRotStart;
    float yRotEnd;

    public RotationState(Character2 owner) : base(owner)
    {
    }

    public override void OnStateEnter()
    {
        timer = 0f;
        yRotStart = camHolderTransform.localEulerAngles.y;
        yRotEnd = yRotStart + owner.camRotAmount;
    }

    public override void OnUpdate()
    {
        Gravity();
        float yRotCur = Mathf.Lerp(yRotStart, yRotEnd, timer / camRotDuration);
        camHolderTransform.localEulerAngles = new Vector3(0f, yRotCur, 0f);
        timer += Time.deltaTime;
        if (timer >= camRotDuration)
        {
            camHolderTransform.localEulerAngles = new Vector3(0f, yRotEnd % 360, 0f);
            owner.SwitchState(owner.normalState);
        }
    }
}
