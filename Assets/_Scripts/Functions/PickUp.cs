﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour
{
    public GameObject Pickup;
    public GameObject PickupToInstanciate;
    public GameObject EquipPosition;
    Transform ts;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag != "Player"){return;}
        Debug.Log("Somebody picked me up: " + other.name);
        //Vector3 pickUpPosition = new Vector3(EquipPosition.transform.position.x, 1.6f, EquipPosition.transform.position.z);
        Vector3 pickUpPosition = EquipPosition.transform.position;

        if (IsPickedPickUp.picked == false)
        {

            GameObject picked = Instantiate(PickupToInstanciate, pickUpPosition, PickupToInstanciate.transform.rotation, EquipPosition.GetComponent<Transform>());
            //picked.transform.localPosition = Vector3.zero;
            IsPickedPickUp.picked = true;
            Pickup.SetActive(false);
            Pickup.GetComponent<PickUp>().enabled = false;
        }
    }
}
