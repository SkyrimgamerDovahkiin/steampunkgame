using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.InputSystem;

public class NPCInteract : MonoBehaviour
{
    public TextMeshProUGUI talkWithNPC;
    public TextMeshProUGUI dialogue;
    public GameObject holder;
    public Vector3 currentFollowerPos;
    public GameObject follower;
    public GameObject pickup;
    public GameObject pickupInstanciate;
    public GameObject EquipPosition;
    public Vector3 pickUpPosition;
    public GameObject DialogueBackground;
    public InputActionAsset inputActions;
    public InputAction interactAction;
    bool dialogueHappened = false;
    public float textWait = 1f;
    int buttonPresses = 0;

    void Start()
    {
        var gameplayActionMap = inputActions.FindActionMap("UI");
        interactAction = gameplayActionMap.FindAction("Interact");
        interactAction.Enable();
    }

    void Update()
    {
        talkWithNPC.enabled = false;
        //RaycastHit hitInfo;
        //{
        talkWithNPC.enabled = true;
        talkWithNPC.text = "Drücke A um mit dem NPC zu reden";

        if (EquipPosition.transform.childCount > 0) 
        {
            Debug.Log("LOLTsesr");
            return; 
        }
        if (interactAction.triggered)
        {
            switch (buttonPresses)
            {
                case 0:
                    DialogueBackground.SetActive(true);
                    dialogue.text = "Bruder, i need your NPC wenn ich dir Teil geben soll";
                    buttonPresses++;
                    break;
                case 1:
                    dialogue.text = "Außerdem bist du ein kompletter Idiot";
                    buttonPresses++;
                    break;
                case 2:
                    DialogueBackground.SetActive(false);
                    StartCoroutine(NPCHolding());
                    buttonPresses++;
                    break;
                default:
                    break;
            }
        }
        //}
    }

    IEnumerator NPCHolding()
    {
        currentFollowerPos = follower.transform.position;
        follower.GetComponent<Vehicle>().enabled = false;
        follower.transform.position = holder.transform.position;
        yield return new WaitForSeconds(1.5f);
        follower.GetComponent<Vehicle>().enabled = true;
        follower.transform.position = currentFollowerPos;
        yield return new WaitForSeconds(0.5f);
        pickUpPosition = EquipPosition.transform.position;
        GameObject picked = Instantiate(pickupInstanciate, pickUpPosition, pickupInstanciate.transform.rotation, EquipPosition.GetComponent<Transform>());
        picked.transform.localPosition = Vector3.zero;
        IsPickedPickUp.picked = true;
        pickup.SetActive(false);
    }
}
