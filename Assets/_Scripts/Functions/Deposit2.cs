using UnityEngine;

public class Deposit2 : MonoBehaviour
{
    public string Name;
    public GameObject DepositPrefab;
    public Transform AblageEmpty;
    public GameObject currentTrigger;
    public GameObject nextTrigger;
    bool deposited = false;
    public Lift Lift;


    private void OnTriggerEnter(Collider other) 
    {
        if(deposited){return;}
        Arrival follower = other.GetComponent<FollowerInteract>().Follower;
        if(follower == null){return;}
        GameObject go = follower.transform.GetChild(0).Find(Name)?.gameObject;
        if(go != null)
        {
            Destroy(go);
            Instantiate(DepositPrefab, AblageEmpty.position, DepositPrefab.transform.rotation, AblageEmpty);
            print("Instantiated " + DepositPrefab.name);
            deposited = true;
            if(nextTrigger != null)
            {
                currentTrigger.SetActive(false);
                nextTrigger.SetActive(true);
            }
            if (nextTrigger == null)
            {
                Lift.enabled = true;
            }
            this.enabled = false;
        }
        IsPickedPickUp.picked = false;
    }
}
