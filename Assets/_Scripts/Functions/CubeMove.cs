using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeMove : MonoBehaviour
{
    public GameObject moveCube;
    public Vector3 positionToMoveTo;
    public BoxCollider front;
    public BoxCollider back;
    public BoxCollider left;
    public BoxCollider right;
    bool isMoving;

    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (hit.collider == front && !isMoving)
        {
            StartCoroutine(LerpPosition(positionToMoveTo, new Vector3(0,0,3), 1));
        }

        if (hit.collider == back && !isMoving)
        {
            StartCoroutine(LerpPosition(positionToMoveTo, new Vector3(0,0,-3), 1));
        }

        if (hit.collider == left && !isMoving)
        {
            StartCoroutine(LerpPosition(positionToMoveTo, new Vector3(3,0,0), 1));
        }

        if (hit.collider == right && !isMoving)
        {
            StartCoroutine(LerpPosition(positionToMoveTo, new Vector3(-3,0,0), 1));
        }
    }

    IEnumerator LerpPosition(Vector3 targetPosition, Vector3 dir, float duration)
    {
        isMoving = true;
        float time = 0;
        Vector3 startPosition = moveCube.GetComponent<Rigidbody>().transform.position;
        targetPosition = moveCube.GetComponent<Rigidbody>().transform.position + dir;

        while (time < duration)
        {
            moveCube.GetComponent<Rigidbody>().transform.position = Vector3.Lerp(startPosition, targetPosition, time / duration);
            time += Time.deltaTime;
            yield return null;
        }
        moveCube.GetComponent<Rigidbody>().transform.position = targetPosition;
        isMoving = false;
    }

    
    // IEnumerator LerpPositionFront(Vector3 targetPosition, float duration)
    // {
    //     float time = 0;
    //     Vector3 startPosition = moveCube.GetComponent<Rigidbody>().transform.position;
    //     targetPosition = moveCube.GetComponent<Rigidbody>().transform.position + new Vector3(0,0,3);

    //     while (time < duration)
    //     {
    //         moveCube.GetComponent<Rigidbody>().transform.position = Vector3.Lerp(startPosition, targetPosition, time / duration);
    //         time += Time.deltaTime;
    //         yield return null;
    //     }
    //     moveCube.GetComponent<Rigidbody>().transform.position = targetPosition;
    // }

    // IEnumerator LerpPositionBack(Vector3 targetPosition, float duration)
    // {
    //     float time = 0;
    //     Vector3 startPosition = moveCube.GetComponent<Rigidbody>().transform.position;
    //     targetPosition = moveCube.GetComponent<Rigidbody>().transform.position + new Vector3(0,0,-3);

    //     while (time < duration)
    //     {
    //         moveCube.GetComponent<Rigidbody>().transform.position = Vector3.Lerp(startPosition, targetPosition, time / duration);
    //         time += Time.deltaTime;
    //         yield return null;
    //     }
    //     moveCube.GetComponent<Rigidbody>().transform.position = targetPosition;
    // }

    // IEnumerator LerpPositionLeft(Vector3 targetPosition, float duration)
    // {
    //     float time = 0;
    //     Vector3 startPosition = moveCube.GetComponent<Rigidbody>().transform.position;
    //     targetPosition = moveCube.GetComponent<Rigidbody>().transform.position + new Vector3(3,0,0);

    //     while (time < duration)
    //     {
    //         moveCube.GetComponent<Rigidbody>().transform.position = Vector3.Lerp(startPosition, targetPosition, time / duration);
    //         time += Time.deltaTime;
    //         yield return null;
    //     }
    //     moveCube.GetComponent<Rigidbody>().transform.position = targetPosition;
    // }

    // IEnumerator LerpPositionRight(Vector3 targetPosition, float duration)
    // {
    //     float time = 0;
    //     Vector3 startPosition = moveCube.GetComponent<Rigidbody>().transform.position;
    //     targetPosition = moveCube.GetComponent<Rigidbody>().transform.position + new Vector3(-3,0,0);

    //     while (time < duration)
    //     {
    //         moveCube.GetComponent<Rigidbody>().transform.position = Vector3.Lerp(startPosition, targetPosition, time / duration);
    //         time += Time.deltaTime;
    //         yield return null;
    //     }
    //     moveCube.GetComponent<Rigidbody>().transform.position = targetPosition;
    // }
}
