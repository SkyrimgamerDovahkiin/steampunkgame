using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.InputSystem;

public class FollowerInteract : MonoBehaviour
{
    public Arrival Follower;
    public TextMeshProUGUI talkWithFollower;
    public KeyCode interact;
    public GameObject menu;
    public InputActionAsset inputActions;
    public InputAction interactAction;

    void Start()
    {
        var gameplayActionMap = inputActions.FindActionMap("UI");
        interactAction = gameplayActionMap.FindAction("Interact");
        interactAction.Enable();
    }

    void Update()
    {
        talkWithFollower.enabled = false;
        //RaycastHit hitInfo;
        //if (Physics.Raycast(transform.position, transform.forward, out hitInfo, 2f) && hitInfo.transform.name == "Follower")
        //{
            talkWithFollower.enabled = true;
            talkWithFollower.text = "Drücke A um mit dem Follower zu reden";

            if (interactAction.triggered)
            {
                Debug.Log("Snickers");
                menu.SetActive(true);
                Time.timeScale = 0;
            }

            if (menu.activeSelf == true)
            {
                talkWithFollower.enabled = false;
            }
        //}
    }
}