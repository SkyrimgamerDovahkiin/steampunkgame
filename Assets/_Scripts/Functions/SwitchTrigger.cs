using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchTrigger : MonoBehaviour
{
    public GameObject cubeToEnable;
    void OnTriggerEnter(Collider other)
    {
        //Anim wird gespielt
        if(other.name == "MoveCube")
        {
            cubeToEnable.SetActive(true);
        }
    }
}
