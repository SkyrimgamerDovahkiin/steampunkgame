using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lift : MonoBehaviour
{
    float timeDown;
    float timeUp;
    public Vector3 startPosition;
    public Vector3 targetPosition;
    float waitTime;
    bool canMove;

    public bool isDown = false;
    void Start()
    {
        timeDown = 0;
        startPosition = transform.position;
        targetPosition = new Vector3(transform.position.x, 3.8f, transform.position.z);
    }
    void FixedUpdate()
    {
        if (!canMove) { return; }
        if (isDown == false)
        {
            while (timeDown < 3)
            {
                transform.position = Vector3.Lerp(startPosition, targetPosition, timeDown / 3);
                timeDown += Time.deltaTime;
                return;
            }
            transform.position = targetPosition;
            while (waitTime < 3f)
            {
                waitTime += Time.deltaTime;
                return;
            }
            //isDown = true;
            waitTime = 0f;
            timeUp = 0;
        }

        // if (isDown == true)
        // {
        //     while (timeUp < 3)
        //     {
        //         transform.position = Vector3.Lerp(targetPosition, startPosition, timeUp / 3);
        //         timeUp += Time.deltaTime;
        //         return;
        //     }
        //     transform.position = startPosition;
        //     isDown = false;
        //     timeDown = 0;
        // }
        // isDown = false;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.transform.parent = transform;
            canMove = true;
        }

        if (other.CompareTag("Follower"))
        {
            other.transform.parent = transform;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.transform.parent = null;
        }

        if (other.CompareTag("Follower"))
        {
            other.transform.parent = null;
        }
    }
}
