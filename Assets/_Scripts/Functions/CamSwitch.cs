using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamSwitch : MonoBehaviour
{
    Camera cam;
    public GameObject TopDownEmpty;
    public GameObject camHolder;
    public Vector3 endPos;
    public Quaternion endRot;
    float duration = 3f;
    void Start()
    {
        cam = Camera.main;
    }

    void OnTriggerEnter(Collider other)
    {
        StartCoroutine(ChangeCamCO());
    }

    IEnumerator ChangeCamCO()
    {
        float time = 0;
        Vector3 startPos = cam.transform.position;
        Quaternion startRot = cam.transform.rotation;
        endPos = TopDownEmpty.transform.position;
        endRot = Quaternion.Euler(90, 0, 0);

        while (time < duration)
        {
            cam.transform.position = Vector3.Lerp(startPos, endPos, time / duration);
            cam.transform.rotation = Quaternion.Slerp(startRot, endRot,  time / duration);
            time += Time.deltaTime;
            yield return null;
        }

        cam.transform.position = endPos;
        cam.transform.rotation = endRot;
        cam.transform.SetParent(TopDownEmpty.transform);
    }
}
