﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC_Follow : MonoBehaviour
{
    public GameObject ThePlayer;
    public float TargetDistance;
    public float AllowedDistance = 0.0000000001f;
    public GameObject TheNPC;
    public float Followspeed;
    public RaycastHit Shot;
    //public GameObject RaycastEmpty; 

    void Update()
    {
        transform.LookAt(ThePlayer.transform);
        if(Physics.Raycast(transform.position,transform.TransformDirection(Vector3.forward),out Shot))
        {
            TargetDistance = Shot.distance;
            if(TargetDistance >= AllowedDistance)
            {

                Followspeed = 0.5f;
                transform.position = Vector3.MoveTowards(transform.position, ThePlayer.transform.position, Followspeed);
            }
            else
            {
            
             Followspeed = 0;
            }
        }
    }
}
