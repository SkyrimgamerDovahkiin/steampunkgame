using UnityEngine;
using TMPro;
using UnityEngine.InputSystem;

public class FollowerMenu : MonoBehaviour
{
    public TextMeshProUGUI option1;
    public TextMeshProUGUI option2;
    public TextMeshProUGUI option3;
    public InputActionAsset inputActions;
    public InputAction navigateAction;
    public InputAction confirmAction;
    public GameObject follower;
    public GameObject pickup1;
    public GameObject pickup2;
    public GameObject pickup3;

    int numberOfOptions = 3;
    int selectedOption;
    bool m_isAxisInUse = false;

    // Use this for initialization
    void Start()
    {
        selectedOption = 1;
        option1.color = new Color32(255, 255, 255, 255);
        option2.color = new Color32(0, 0, 0, 255);
        option3.color = new Color32(0, 0, 0, 255);

        var gameplayActionMap = inputActions.FindActionMap("UI");
        navigateAction = gameplayActionMap.FindAction("Navigate");
        confirmAction = gameplayActionMap.FindAction("Confirm");
        confirmAction.Enable();
        navigateAction.Enable();
    }

    void Update()
    {
        if (navigateAction.ReadValue<Vector2>().y < 0f)
        {
            if (m_isAxisInUse == false)
            {
                selectedOption += 1;
                if (selectedOption > numberOfOptions)
                {
                    selectedOption = 1;
                }
                m_isAxisInUse = true;
            }

            option1.color = new Color32(0, 0, 0, 255);
            option2.color = new Color32(0, 0, 0, 255);
            option3.color = new Color32(0, 0, 0, 255);

            switch (selectedOption)
            {
                case 1:
                    option1.color = new Color32(255, 255, 255, 255);
                    break;
                case 2:
                    option2.color = new Color32(255, 255, 255, 255);
                    break;
                case 3:
                    option3.color = new Color32(255, 255, 255, 255);
                    break;
            }
        }

        if (navigateAction.ReadValue<Vector2>().y > 0f)
        {
            if (m_isAxisInUse == false)
            {
                selectedOption -= 1;
                if (selectedOption < 1)
                {
                    selectedOption = numberOfOptions;
                }
                m_isAxisInUse = true;
            }

            option1.color = new Color32(0, 0, 0, 255);
            option2.color = new Color32(0, 0, 0, 255);
            option3.color = new Color32(0, 0, 0, 255);

            switch (selectedOption)
            {
                case 1:
                    option1.color = new Color32(255, 255, 255, 255);
                    break;
                case 2:
                    option2.color = new Color32(255, 255, 255, 255);
                    break;
                case 3:
                    option3.color = new Color32(255, 255, 255, 255);
                    break;
            }
        }

        navigateAction.performed += ctx => m_isAxisInUse = false;

        if (confirmAction.triggered)
        {
            Time.timeScale = 1;
            gameObject.SetActive(false);

            switch (selectedOption)
            {
                case 1:
                    follower.GetComponent<Vehicle>().enabled = false;
                    break;
                case 2:
                    follower.GetComponent<Vehicle>().enabled = true;
                    break;
                case 3:
                    if (GameObject.Find("BatteryToInstanciate(Clone)"))
                    {
                        Destroy(GameObject.Find("BatteryToInstanciate(Clone)"));
                        pickup1.transform.position = follower.transform.position;
                        pickup1.SetActive(true);
                        IsPickedPickUp.picked = false;
                    }

                    if (GameObject.Find("GluehbirneToInstanciate(Clone)"))
                    {
                        Destroy(GameObject.Find("GluehbirneToInstanciate(Clone)"));
                        pickup2.transform.position = follower.transform.position;
                        pickup2.SetActive(true);
                        IsPickedPickUp.picked = false;
                    }

                    if (GameObject.Find("GearToInstanciate(Clone)"))
                    {
                        Destroy(GameObject.Find("GearToInstanciate(Clone)"));
                        pickup3.transform.position = follower.transform.position;
                        pickup3.SetActive(true);
                        IsPickedPickUp.picked = false;
                    }
                    break;
            }
        }
    }
}
