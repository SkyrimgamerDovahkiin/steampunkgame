using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Talk : MonoBehaviour
{
    public NPCInteract NPCInteract;
    public FollowerInteract FollowerInteract;
    public TextMeshProUGUI talkWithFollower;
    public TextMeshProUGUI talkWithNPC;
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Follower"))
        {
            FollowerInteract.enabled = true;
        }

        if (other.CompareTag("NPC"))
        {
            NPCInteract.enabled = true;
            if (FollowerInteract.enabled == true)
            {
                FollowerInteract.enabled = false;
                talkWithFollower.enabled = false;
            }
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("NPC"))
        {
            if (FollowerInteract.enabled == true)
            {
                FollowerInteract.enabled = false;
                talkWithFollower.enabled = false;
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Follower"))
        {
            FollowerInteract.enabled = false;
            talkWithFollower.enabled = false;
        }

        if (other.CompareTag("NPC"))
        {
            NPCInteract.enabled = false;
            talkWithNPC.enabled = false;
        }
    }
}
