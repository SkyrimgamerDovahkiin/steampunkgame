using UnityEngine;
using System.Collections.Generic;
using Random = System.Random;

public static class ExtensionMethods
{
    public static Vector2 To2d (this Vector3 value)
    {
        return new Vector2(value.x, value.z);
    }

    public static Vector3 To3d (this Vector2 value)
    {
        return new Vector3(value.x, 0f, value.y);
    }

    private static Random rng = new Random();

    public static void Shuffle<T>(this IList<T> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }
}
