using UnityEngine;

public class Arrival : SteeringBehaviour
{
    [SerializeField] float slowingDistance = 7f;
    [SerializeField] float stopDistance = 2f;
    [SerializeField] AnimationCurve curve;

    public override Vector2 Steer(
        Transform target, 
        Vector3 velocity, 
        float maxSpeed, 
        float maxForce)
    {
        Vector2 dirToTarget = target.position.To2d() - transform.position.To2d();
        float distance = dirToTarget.magnitude;
        dirToTarget = Vector2.ClampMagnitude(dirToTarget, distance - stopDistance);
        distance = distance - stopDistance;
        float t = distance / slowingDistance;
        t = curve.Evaluate(t);
        Vector2 desired = Mathf.Lerp(0f, maxSpeed, t) * dirToTarget.normalized;
        Vector2 steeringForce = desired - velocity.To2d();
        steeringForce = Vector2.ClampMagnitude(steeringForce, maxForce);
        return steeringForce;
    }
}
