using UnityEngine;

public abstract class SteeringBehaviour : MonoBehaviour
{
    [SerializeField] protected float speedModifier = 1f;
    [SerializeField] protected float forceModifier = 1f;

    public abstract Vector2 Steer(
        Transform target, 
        Vector3 velocity, 
        float maxSpeed, 
        float maxForce);
}
